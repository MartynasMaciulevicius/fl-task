(ns fl-task.ui
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs-http.client :as http]
            [clojure.string :as strs]
            [cljs.core.async :refer [<!]]))

(defn find-scramble-input-elem []
  (document/getElementById "scramble-input"))

(defn find-word-input-elem []
  (document/getElementById "word-input"))

(defn find-output-elem []
  (document/getElementById "output"))

(defn set-output-elem-text [text is-red]
  (let [output-elem (find-output-elem)]
    (set! (.-style output-elem)
          (if is-red
            "color:tomato;"
            ""))
    (set! (.-innerText output-elem) text)))

(defn fetch-scramble [scrambled-letters word]
  (http/get "http://localhost:4000/check-scramble"
            {:query-params {"filter-letters" scrambled-letters
                            "word" word}}))

(defn handle-check-button-click []
  (go (let [response (<! (fetch-scramble (.-value (find-scramble-input-elem))
                                         (.-value (find-word-input-elem))))]
        (cond
          (not= (:status response)
                200) (set-output-elem-text "Can't reach the server." true)
          (not (boolean? (:body response))) (set-output-elem-text "Unknown response." true)
          (:body response) (set-output-elem-text
                            "Word can be rearranged to become part of scramble." false)
          :else (set-output-elem-text
                 "Word can't be rearranged to become part of scramble." false)))))

(defn -main []
  (.addEventListener (document/getElementById "check-scramble-button")
                     "click"
                     (fn [] (handle-check-button-click))))

(-main)
