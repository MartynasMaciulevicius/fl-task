(ns fl-task.core
  (:require [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer :all]
            [schema.core :as s]
            [fl-task.scramble :as scramble]
            [jumblerg.middleware.cors :refer [wrap-cors]]))

(s/def ScrambleResponse s/Bool)

(def app
  (->
   (api

    {:swagger
     {:ui "/"
      :spec "/swagger.json"
      :data {:info {:title "Sample API"
                    :description "Compojure Api example"}
             :tags [{:name "api", :description "some apis"}]
             :consumes ["application/json"]
             :produces ["application/json"]}}}

    (GET "/check-scramble" []
      :query-params [filter-letters :- String
                     word :- String]
      :return ScrambleResponse
      (ok (scramble/scramble? filter-letters word))))
   (wrap-cors #".*")))
