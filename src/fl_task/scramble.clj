(ns fl-task.scramble
  (:import java.util.Arrays))

(defn- to-sorted-bytes [scrambled-letters]
  (let [char-bytes (.getBytes scrambled-letters)]
    ;; Java copies the array so it's fine to sort this one
    (Arrays/sort char-bytes)
    char-bytes))

(defn scramble? [scrambled-letters word]
  (let [sorted-scramble-bytes (to-sorted-bytes scrambled-letters)
        sorted-word-bytes (to-sorted-bytes word)
        sorted-scramble-bytes-count (count sorted-scramble-bytes)
        sorted-word-bytes-count (count sorted-word-bytes)]
    (loop [scramble-idx 0
           word-idx 0]
      (cond
        (= sorted-word-bytes-count word-idx) true
        (= sorted-scramble-bytes-count scramble-idx) false
        (= (aget sorted-scramble-bytes scramble-idx)
           (aget sorted-word-bytes word-idx)) (recur (inc scramble-idx)
                                                     (inc word-idx))
        :else (recur (inc scramble-idx)
                     word-idx)))))
