(defproject fl-task "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [metosin/compojure-api "1.1.13"]
                 [org.clojure/clojurescript "1.10.597"]
                 [cljs-http "0.1.46"]
                 [org.clojure/core.async "1.1.582"]
                 [jumblerg/ring-cors "2.0.0"]]

  ;; These dependencies should be included in dev-config but for example's sake it's here.
  :plugins [[lein-ring "0.12.5"]
            [lein-cljsbuild "1.1.7"]]

  :ring {:handler fl-task.core/app}

  :clean-targets
  [[:cljsbuild :builds 0 :compiler :output-to]
   :target-path
   :compile-path]
  :cljsbuild {:builds
              [{:id "dev"
                :source-paths ["src"]
                :compiler {:output-dir "out"
                           :output-to "index-dev.js"
                           :optimizations :none
                           :source-map true}}
               {:id "prod"
                :source-paths ["src"]
                :compiler {:output-to "index-prod.js"
                           :optimizations :whitespace}}]}

  :repl-options {:init-ns fl-task.core})
