(ns fl-task.scramble-test
  (:require [fl-task.scramble :as sut]
            [clojure.test :refer :all]))

(deftest scramble?-test-exapmle-cases
  (testing "should cover first example case"
    (is (sut/scramble? "rekqodlw" "world")))
  (testing "should cover second example case"
    (is (sut/scramble? "cedewaraaossoqqyt" "codewars")))
  (testing "should cover third example case"
    (is (not (sut/scramble? "katas"  "steak")))))

(deftest scramble?-test
  (testing "should accept same letters"
    (is (sut/scramble? "aaaaa" "aaaaa")))
  (testing "should not accept same letters but with longer word"
    (is (not (sut/scramble? "aaaaa" "aaaaaaaa"))))
  (testing "should not accept when one letter is missing; one less 'a' in scramble"
    (is (not (sut/scramble? "aaaa" "aaaaa"))))
  (testing "should not accept when one letter is missing; 'b' missing in word"
    (is (not (sut/scramble? "aaaab" "aaaaa"))))
  (testing "should accept when scramble has more letters; extra 'a' in scramble"
    (is (sut/scramble? "caaaab" "acaba")))
  (testing "should accept empty word"
    (is (sut/scramble? "hi" "")))
  (testing "should reject empty scramble"
    (is (not (sut/scramble? "" "nope"))))
  (testing "should accept when both empty"
    (is (sut/scramble? "" ""))))

